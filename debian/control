Source: kproperty
Section: libs
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Pino Toscano <pino@debian.org>
Build-Depends: cmake (>= 3.0),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 1.8.0),
               libkf5config-dev (>= 5.16.0),
               libkf5coreaddons-dev (>= 5.16.0),
               libkf5guiaddons-dev (>= 5.16.0),
               libkf5i18n-dev (>= 5.16.0),
               libkf5widgetsaddons-dev (>= 5.16.0),
               pkg-kde-tools (>= 0.15.16),
               qttools5-dev,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://community.kde.org/KProperty
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kproperty.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kproperty

Package: libkpropertycore3-4
Architecture: any
Multi-Arch: same
Depends: libkproperty-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: property editing framework -- core library
 KProperty is a property editing framework with editor widget similar
 to what is known from Qt Designer.
 .
 This package contains the shared core library.
 .
 This package is part of the Calligra Suite.

Package: libkpropertywidgets3-4
Architecture: any
Multi-Arch: same
Depends: libkproperty-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: property editing framework -- widgets library
 KProperty is a property editing framework with editor widget similar
 to what is known from Qt Designer.
 .
 This package contains the shared widgets library.
 .
 This package is part of the Calligra Suite.

Package: libkproperty-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: data files for KProperty
 KProperty is a property editing framework with editor widget similar
 to what is known from Qt Designer.
 .
 This package contains the architecture independent data files for the
 KProperty libraries.
 .
 This package is part of the Calligra Suite.

Package: libkproperty3-dev
Architecture: any
Section: libdevel
Depends: libkpropertycore3-4 (= ${binary:Version}),
         libkpropertywidgets3-4 (= ${binary:Version}),
         qtbase5-dev,
         libkf5widgetsaddons-dev,
         ${misc:Depends},
Description: development files for KProperty
 KProperty is a property editing framework with editor widget similar
 to what is known from Qt Designer.
 .
 This package contains the development files for the KProperty libraries.
 .
 This package is part of the Calligra Suite.
